import java.io.IOException;
import java.util.Scanner;

public class TicTacToe {

    public static void main(String[] args) throws IOException {
        Scanner reader = new Scanner(System.in);
        char[][] board = {{' ', ' ', ' '}, {' ', ' ', ' '}, {' ', ' ', ' '}};

        printBoard(board);
        boolean flag = false;
        boolean draw = true;

        over:
        while (true) {
            outer:
            for (int i = 0; true; i++) {
                for (int j = 0; j < 3; j++) {
                    if (board[i][j] == ' ') break outer;
                    flag = true;
                }
                if (i == 2) break over;
            }
            System.out.print("Player 1 enter row number:");
            int row = reader.nextInt();
            System.out.print("Player 1 enter column number:");
            int col = reader.nextInt();

            while (row <= 0 || col <= 0 || row > 3 || col > 3) {
                System.out.println(" Location is out of range please write another coordinate!");
                System.out.print("Player 1 enter row number:");
                row = reader.nextInt();
                System.out.print("Player 1 enter column number:");
                col = reader.nextInt();
            }
            while (board[row - 1][col - 1] != ' ') {
                System.out.println(" Given location is already occupied please write another coordinate!");
                System.out.print("Player 1 enter row number:");
                row = reader.nextInt();
                System.out.print("Player 1 enter column number:");
                col = reader.nextInt();
                while (row <= 0 || col <= 0 || row > 3 || col > 3) {
                    System.out.println(" Location is out of range please write another coordinate!");
                    System.out.print("Player 2 enter row number:");
                    row = reader.nextInt();
                    System.out.print("Player 2 enter column number:");
                    col = reader.nextInt();
                }
            }


            board[row - 1][col - 1] = 'X';

            printBoard(board);
            if (checkBoard(board)) {
                System.out.println("Player 1 win , Game Over!!");
                draw = false;
                break;
            }

            outer:
            for (int i = 0; true; i++) {
                for (int j = 0; j < 3; j++) {
                    if (board[i][j] == ' ') break outer;
                    flag = true;
                }
                if (i == 2) break over;
            }
            System.out.print("Player 2 enter row number:");
            row = reader.nextInt();
            System.out.print("Player 2 enter column number:");
            col = reader.nextInt();
            while (row <= 0 || col <= 0 || row > 3 || col > 3) {
                System.out.println(" Location is out of range please write another coordinate!");
                System.out.print("Player 2 enter row number:");
                row = reader.nextInt();
                System.out.print("Player 2 enter column number:");
                col = reader.nextInt();
            }
            while (board[row - 1][col - 1] != ' ') {
                System.out.println(" Given location is already occupied please write another coordinate!");
                System.out.print("Player 2 enter row number:");
                row = reader.nextInt();
                System.out.print("Player 2 enter column number:");
                col = reader.nextInt();
                while (row <= 0 || col <= 0 || row > 3 || col > 3) {
                    System.out.println(" Location is out of range please write another coordinate!");
                    System.out.print("Player 2 enter row number:");
                    row = reader.nextInt();
                    System.out.print("Player 2 enter column number:");
                    col = reader.nextInt();
                }
            }


            board[row - 1][col - 1] = 'O';
            printBoard(board);
            if (checkBoard(board)) {
                System.out.println("Player 2 win , Game Over!!");
                draw = false;
                break;
            }


        }
        if (draw) System.out.println(" DRAW!! , Game Over!!!");
        reader.close();
    }

    public static void printBoard(char[][] board) {
        System.out.println("    1   2   3");
        System.out.println("   -----------");
        for (int row = 0; row < 3; ++row) {
            System.out.print(row + 1 + " ");
            for (int col = 0; col < 3; ++col) {
                System.out.print("|");
                System.out.print(" " + board[row][col] + " ");
                if (col == 2)
                    System.out.print("|");

            }
            System.out.println();
            System.out.println("   -----------");

        }

    }

    public static boolean checkBoard(char[][] board) {
        boolean flag = false;
        for (int i = 0; i < 3; i++) {
            // row state
            if (board[i][0] == board[i][1] && board[i][0] == board[i][2]) {
                if (board[i][0] != ' ') flag = true;
                break;
            }
            // column state
            if (board[0][i] == board[1][i] && board[0][i] == board[2][i]) {
                if (board[0][i] != ' ') flag = true;
                break;
            }
            // cross state
            if (board[0][0] == board[1][1] && board[1][1] == board[2][2]) {

                if (board[0][0] != ' ') flag = true;
                break;
            }
            if (board[0][2] == board[1][1] && board[0][2] == board[2][0]) {
                if (board[0][2] != ' ') flag = true;
                break;
            }
        }
        return flag;

    }


}